
function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	if(rowCount < 10){							// maksimaalselt 10 välja
		var row = table.insertRow(rowCount);
		var colCount = table.rows[0].cells.length;
		for(var i=0; i<colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[0].cells[i].innerHTML;
		}
	}else{
		 alert("Maksimaalne arv välju 10");
			   
	}
}


function deleteRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;	
			if(rowCount < 2) { 						// üle ühe välja peab olema
				alert("Peab üle ühe välja olema");
			}
			if(rowCount > 1) { 						
				table.deleteRow(rowCount-1);	
			}	
}