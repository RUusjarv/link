<?php

function connect_db(){
  global $connection;
  $host="localhost";
  $user="test";
  $pass="*********";
  $db="test";
  $connection = mysqli_connect($host, $user, $pass, $db) or die("Ei saa mootoriga ühendust");
  mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}

function show_vote(){
		include("Link.html");
		show_result();
}

function show_result(){
		$errors=array();
		$allData=array(array());
		$URLs=different();
		for ($i=0; $i<count($URLs); $i++){//kõik lehed
		$allData[$i][0] =$URLs[$i];	
		
		// lõpp hinne
		$URLgrades=grades($URLs[$i]);
		$x=count(grades($URLs[$i]));
		$totalPoints = 0;
			for ($j=0; $j<$x; $j++){
				$totalPoints= $URLgrades[$j] + $totalPoints;
			}
				$allData[$i][1] = $totalPoints;
				
		// kõik kategooriad
		$allCatecorys = null;
		$URLcatecorys=catecory($URLs[$i]);
		$x=count(catecory($URLs[$i]));
			for ($j=0; $j<$x; $j++){
				$allCatecorys[]= $URLcatecorys[$j];
			}	
				$duplicates = removeDuplicates($allCatecorys);
				$allData[$i][2] = implode($duplicates, ", ");
			
		
		// kõik omanikud
		$allOwners = null;
		$URLOwners=owner($URLs[$i]);
		$x=count(owner($URLs[$i]));
			for ($j=0; $j<$x; $j++){
				$allOwners[]= $URLOwners[$j];
			}	
				$duplicates = removeDuplicates($allOwners);
				$allData[$i][3] = implode($duplicates, ", ");
		
		
		// kõik kirjeldused
		$allDescriptions = null;
		$URLdescriptions=description($URLs[$i]);
		$x=count(description($URLs[$i]));
			for ($j=0; $j<$x; $j++){
				$allDescriptions[]= $URLdescriptions[$j];
			}	
				$duplicates = removeDuplicates($allDescriptions);
				$allData[$i][4] = implode($duplicates, ", ");
				
				
		// kõik pealkirjad
		$allNames = null;
		$URLNames=name($URLs[$i]);
		$x=count(name($URLs[$i]));
			for ($j=0; $j<$x; $j++){
				$allNames[]= $URLNames[$j];
			}	
				$duplicates = removeDuplicates($allNames);
				$allData[$i][5] = implode($duplicates, ", ");	

		}
		//sorteeri punktide järgi
		usort($allData, function($a, $b) {
				return $b[1] - $a[1];
			});
		include_once("Zelda.html");		
}
function removeDuplicates($input){
	$lowercases = array_map('strtolower', $input);
	$duplicates = array_unique($lowercases);
	return $duplicates;
}
function different(){
	global $connection;
	$different=array();
	
	$query ="SELECT DISTINCT URL FROM ruusjarv_veebilehed";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$different[]=$row['URL'];
	}
	return $different;
}
function grades($URL){
	global $connection;
	$grades=array();
	
	$URL=mysqli_real_escape_string($connection, $URL);
	
	$query ="SELECT punktid FROM ruusjarv_veebilehed WHERE URL='$URL'";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$grades[]=$row['punktid'];

	}
	return $grades;
}
function catecory($URL){
	global $connection;
	$catecory=array();
	
	$URL=mysqli_real_escape_string($connection, $URL);
	
	$query ="SELECT kategooria FROM ruusjarv_veebilehed WHERE URL='$URL'";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$catecory[]=$row['kategooria'];

	}
	return $catecory;
}
function owner($URL){
	global $connection;
	$owner=array();
	
	$URL=mysqli_real_escape_string($connection, $URL);
	
	$query ="SELECT omanik FROM ruusjarv_veebilehed WHERE URL='$URL'";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$owner[]=$row['omanik'];

	}
	return $owner;
}
function description($URL){
	global $connection;
	$description=array();
	
	$URL=mysqli_real_escape_string($connection, $URL);
	
	$query ="SELECT kirjeldus FROM ruusjarv_veebilehed WHERE URL='$URL'";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$description[]=$row['kirjeldus'];

	}
	return $description;
}
function name($URL){
	global $connection;
	$name=array();
	
	$URL=mysqli_real_escape_string($connection, $URL);
	
	$query ="SELECT pealkiri FROM ruusjarv_veebilehed WHERE URL='$URL'";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$name[]=$row['pealkiri'];

	}
	return $name;
}
function vote(){
	
	$correctFields=array();
	$errors=array();
	if (isset($_POST["fname"]) && $_POST["fname"]!="") {
		$firstName=$_POST["fname"];
	} else {
		$errors[]="Nimi puudu";
	}
	if (isset($_POST["lname"]) && $_POST["lname"]!="") {
		$lastName=$_POST["lname"];
	} else {
		$errors[]="Perekonnanimi puudu";	
	}
	if (isset($_POST["bdate"]) && $_POST["bdate"]!="") {
		$birthday=$_POST["bdate"];
	} else {
		$errors[]="Sünniaeg puudu";	
	}
	if(empty($errors) && check_voted($firstName, $lastName, $birthday)){
		$errors[]="Sa oled juba hääletanud";	
	}
	if (empty($errors)){
		//erinevad välja testid vaja mõelda
		//1. Kas üldse piisav kogus veerge lahti
		if (count($_POST["pname"])<5){
			$errors[]="Järgmine kord tee piisavalt veerge lahti, et saaksid piisava koguse välju ära täita";
		}
		//2. Kas piisav kogus erinevaid nõutuid välju täidetud. 
		else if (count(array_filter($_POST["pname"]))<5){
			$errors[]="Liiga vähe pealkirju";
		}
		else if (count(array_filter($_POST["url"]))<5){
			$errors[]="Liiga vähe URL-e";
		}
		else if (count(array_filter($_POST["category"]))<5){
			$errors[]="Liiga vähe kategooriaid";
		}
		else if (count(array_filter($_POST["grade"]))<5){
			$errors[]="Liiga vähe hindeid pandud";
		}
		//3. Ei oleks läbi segi, piisavalt nõutud välju täidetud
		if (empty($errors)){
			$count = count($_POST["pname"]); // avatud välju
			$x=-1;
			for ($i = 0; $i < $count; $i++){
				if (!empty($_POST["pname"][$i]) && !empty($_POST["url"][$i]) && !empty($_POST["category"][$i]) && !empty($_POST["grade"][$i])){
					$x++;
					for ($j=0; $j < $count; $j++){
					if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$_POST["url"][$i])) {
						$errors[]="Vigane URL";
						$i=$count;
						$j=$count;
						}
					if (empty($errors) && strtolower($_POST["url"][$i]) == strtolower($_POST["url"][$j]) && $i !== $j){ //URL case check
							$errors[]="Ei tohi sama URL poolt hääletada";
							$i=$count;
							$j=$count;
						}						
					}
					if (empty($errors)){
						$correctFields[]=$i;
						}
				}
			}
			if ($x < 4){
				$errors[]="Liiga vähe korrektselt täidetud välju";
				}
			if ($x > 3 && empty($errors)){
				$correctCount = count($correctFields)-1;
				//4. Kui kuskil ei eksinud, siis tegi hääletaja kõik õigelt. Nüüd võiks ka andmed anmebaasi lisada
				add_voter($firstName, $lastName, $birthday); //lisan hääletaja
				for ($z =0; $z < $correctCount; $z++){
					//Lisan hääled
					$nr= $correctFields[$z];
					$headline = $_POST["pname"][$nr];
					$url = $_POST["url"][$nr];
					$description = $_POST["description"][$nr];
					$owner = $_POST["owner"][$nr];
					$catecory = $_POST["category"][$nr];
					$grade = $_POST["grade"][$nr];
					add_site($headline, $url, $description, $owner, $catecory, $grade);
				}
				header('Location: index.php?mode=zelda');
			}
		}
	include_once("Link.html");
		}
	else{
		include_once("Link.html");
		}
}
function add_voter($firstName, $lastName, $birthday) {
	global $connection;

	$firstName=mysqli_real_escape_string($connection, $firstName);
	$lastName=mysqli_real_escape_string($connection, $lastName);
	$birthday=mysqli_real_escape_string($connection, $birthday);

	$query ="insert into ruusjarv_hindajad (firstName, lastName, birthday) values ('$firstName', '$lastName', '$birthday')";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));		
}
function check_voted($firstName, $lastName, $birthday){ 
	global $connection;

	$firstName=mysqli_real_escape_string($connection, $firstName);
	$lastName=mysqli_real_escape_string($connection, $lastName);
	$birthday=mysqli_real_escape_string($connection, $birthday);

	$query ="SELECT * FROM ruusjarv_hindajad WHERE firstName='$firstName' AND lastName='$lastName' AND birthday='$birthday'" ;
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	if (mysqli_num_rows($result)>0) {
		return true;
	}else { 
		return false;
	}
}
function add_site($headline, $url, $description, $owner, $catecory, $grade) {
	global $connection;

	$headline=mysqli_real_escape_string($connection, $headline);
	$url=mysqli_real_escape_string($connection, $url);
	$description=mysqli_real_escape_string($connection, $description);
	$owner=mysqli_real_escape_string($connection, $owner);
	$catecory=mysqli_real_escape_string($connection, $catecory);
	$grade=mysqli_real_escape_string($connection, $grade);
	

	$query ="insert into ruusjarv_veebilehed (pealkiri, URL, kirjeldus, omanik, kategooria, punktid) values ('$headline', '$url', '$description', '$owner', '$catecory', '$grade')";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));		
}


?>